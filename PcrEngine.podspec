Pod::Spec.new do |s|

  s.name         = "PcrEngine"
  s.version      = "1.1.1"
  s.summary      = "快查，信用报告SDK PcrEngine."

  s.description  = <<-DESC
                   DESC

  s.homepage     = "https://github.com/zgtios/PcrEngine.git"

  s.license      = "Apache License, Version 2.0"

  s.author       = { "赵国腾" => "1223019346@qq.com" }

  s.platform     = :ios
  s.platform     = :ios, "7.0"

  s.ios.deployment_target = "7.0"

  s.source       = { :git => "https://git.coding.net/Boy_iOS/PcrEngine.git", :tag => "1.1.1" }

  s.source_files  = " PcrEngine", "PcrEngine/*.{h,m}"
  s.resources = "PcrEngine/ImageMaterial.bundle"
  s.frameworks = "MobileCoreServices.framework", "SystemConfiguration.framework", "AVFoundation.framework"
  s.libraries = "libz.1.2.5.tbd","libxml2.2.tbd"
  s.preserve_paths = 'PcrEngine/PcrEngine.a'

end