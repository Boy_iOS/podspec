#为自己的项目创建 podspec 文件

我们可以为自己的开源项目创建podspec文件，首先通过如下命令初始化一个podspec文件：

    pod spec create your_pod_spec_name
该命令执行之后，CocoaPods 会生成一个名为your_pod_spec_name.podspec的文件，然后我们修改其中的相关内容即可。